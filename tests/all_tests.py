import base_test
class TestCases(base_test.BaseTest):
    def test_01_test_login_page_login(self):
    # Step 1: click login & login page loads
        assert self.welcomepage.click_login(self.loginpage)
        assert self.loginpage.login(self.mainpage,self.param.username,self.param.password)
    # Explanation for above line - 'self' imports the variable from where it's defined - all tests imports base test
    # Base test imports params from utils - param.username is defined in utils
